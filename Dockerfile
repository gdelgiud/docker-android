FROM eclipse-temurin:17

ENV ANDROID_COMPILE_SDK 34
ENV ANDROID_BUILD_TOOLS 34.0.0
ENV ANDROID_COMMAND_LINE_TOOLS   11076708
ENV ANDROID_SDK_ROOT    $PWD/android-sdk-linux
ENV PATH                $PATH:$PWD/android-sdk-linux/platform-tools/

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget unzip
RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_COMMAND_LINE_TOOLS}_latest.zip
RUN unzip -d android-sdk-linux android-sdk.zip
RUN mv $ANDROID_SDK_ROOT/cmdline-tools $ANDROID_SDK_ROOT/tmp
RUN mkdir $ANDROID_SDK_ROOT/cmdline-tools
RUN mv $ANDROID_SDK_ROOT/tmp/ $ANDROID_SDK_ROOT/cmdline-tools/tools
RUN echo y | $ANDROID_SDK_ROOT/cmdline-tools/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
RUN echo y | $ANDROID_SDK_ROOT/cmdline-tools/tools/bin/sdkmanager "platform-tools" >/dev/null
RUN echo y | $ANDROID_SDK_ROOT/cmdline-tools/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
  # temporarily disable checking for EPIPE error and use yes to accept all licenses
SHELL ["/bin/bash", "+o", "pipefail", "-c"]
RUN yes | $ANDROID_SDK_ROOT/cmdline-tools/tools/bin/sdkmanager --licenses
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
